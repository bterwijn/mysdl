#ifndef MYSDL_H_INCLUDED
#define MYSDL_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>

#include <stdlib.h>
#include <iostream>
#include <string>
#include <memory>

#define mysdl_error(error) mysdl_error_help(error,__FILE__,__LINE__)

void mysdl_error_help(std::string error,std::string file,int line)
{ std::cerr<<"mysdl_error "<<error<<" in "<<file<<':'<<line<<' '<<SDL_GetError()<<std::endl; exit(1);}

class MySDL
{
public:

    // using Singleton Design Pattern
    static MySDL* create_window(std::string title="MyWindow",
                           int width=640,
                           int height=480,
                           std::string font="/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf",
                           unsigned fontSize=28)
    {
        if (_instance==NULL)
            _instance=new MySDL(title,width,height,font,fontSize);
        return _instance;
    }

    static void destroy_window()
    {
        if (_instance!=NULL)
        {
            delete _instance;
            _instance=NULL;
        }
    }

    static MySDL* get_window()
    {   return _instance; }

    SDL_Window* window()
    { return _window;}
    
    SDL_Renderer* renderer()
    { return _renderer;}
        
    TTF_Font *font()
    { return _font;}

    int width()
    { return _width;}

    int height()
    { return _height;}

    void getSize(int& w,int& h)
    { w=_width;h=_height;}

    SDL_Point size()
    { return SDL_Point{_width,_height}; }

    SDL_Rect rect()
    { return SDL_Rect{0,0,_width,_height}; }
    
    void handleEvent(SDL_Event& e)
    {
        switch( e.window.event )
        {
            case SDL_WINDOWEVENT_SIZE_CHANGED:
                _width=e.window.data1;
                _height=e.window.data2;
            break;
        }
    }
    
private:
    SDL_Window* _window=NULL;
    SDL_Renderer* _renderer=NULL;
    TTF_Font *_font=NULL;
    int _width,_height;
    
    static MySDL* _instance;

    MySDL(std::string title,int width,int height,
          std::string font,unsigned fontSize)
    {
        _width=width;
        _height=height;
        if (SDL_Init(SDL_INIT_VIDEO)<0) mysdl_error("SDL_Init");
        if ((_window=SDL_CreateWindow(title.c_str(),
                                      SDL_WINDOWPOS_UNDEFINED,
                                      SDL_WINDOWPOS_UNDEFINED,
                                      _width,
                                      _height,
                                      SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE))==NULL)  mysdl_error("SDL_CreateWindow");
        if ((_renderer=SDL_CreateRenderer(_window,-1,SDL_RENDERER_ACCELERATED))==NULL)  mysdl_error("SDL_CreateRenderer");
        int imgFlags = IMG_INIT_PNG;
        if (!( IMG_Init( imgFlags ) & imgFlags ) )  mysdl_error("IMG_Init");
        if (TTF_Init()<0) mysdl_error("TTF_Init");
        if (font.length()>0)
        {
            if ((_font=TTF_OpenFont(font.c_str(),fontSize))==NULL)  mysdl_error("");
        }
        std::cout<<"MySDL initialized"<<std::endl;
    }

    ~MySDL()
    {
        TTF_CloseFont(_instance->_font);
        SDL_DestroyRenderer(_instance->_renderer);
        SDL_DestroyWindow(_instance->_window);
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
        std::cout<<"MySDL destroyed"<<std::endl;
    }
    
    MySDL(const MySDL &) { }
    MySDL& operator=(const MySDL &) { return *this; }
};
MySDL* MySDL::_instance=NULL;

MySDL* sdl()
{   return MySDL::get_window(); }

typedef Uint32 Col;
Col MyColor(Uint8 r,Uint8 g,Uint8 b,Uint8 a)
{
    Uint8 c[]={r,g,b,a};
    return *((Col*)c);
}
    
class MyTexture
{
public:

    MyTexture() : _texture{nullptr}
    {}
    
    ~MyTexture()
    {
        if (_texture!=nullptr)
            SDL_DestroyTexture(_texture);
    }
    
    MyTexture(const MyTexture&) = delete;
    MyTexture(const MyTexture&& m)
    { _texture=m._texture; }
    
    MyTexture& operator=(const MyTexture&) = delete;
    MyTexture& operator=(MyTexture&& m)
    {
        if (_texture!=nullptr)
            SDL_DestroyTexture(_texture);
        _texture=m._texture;
        m._texture=nullptr;
        return *this;
    }
    
    MyTexture(std::string filename)
    {
        if (sdl())
        {
            SDL_Surface* surface;
            if ((surface=IMG_Load(filename.c_str()))==NULL) mysdl_error("IMG_Load");
            makeTexture(surface);
            SDL_FreeSurface(surface);
        }
    }
    
    MyTexture(std::string text,SDL_Color color)
    {
        if (sdl())
        {
            SDL_Surface* surface;
            if ((surface=TTF_RenderText_Solid(sdl()->font(),text.c_str(),color))==NULL) mysdl_error("TTF_RenderText_Solid");
            makeTexture(surface);
            SDL_FreeSurface(surface);
        }
    }
    
    MyTexture(SDL_Window* window)
    {
        if (sdl())
        {
            SDL_Surface* surface;
            if ((surface=SDL_GetWindowSurface(window))==NULL) mysdl_error("SDL_GetWindowSurface");
            int pitch=surface->w*surface->format->BytesPerPixel;
            auto pixels=std::make_unique<unsigned char[]>(surface->h*pitch);
            if (SDL_RenderReadPixels(sdl()->renderer(),NULL,surface->format->format,pixels.get(),pitch)<0) mysdl_error("SDL_RenderReadPixels");
            SDL_Surface* saveSurface;
            if ((saveSurface = SDL_CreateRGBSurfaceFrom(pixels.get(),
                                                        surface->w,
                                                        surface->h,
                                                        surface->format->BitsPerPixel,
                                                        pitch,
                                                        surface->format->Rmask,
                                                        surface->format->Gmask,
                                                        surface->format->Bmask,
                                                        surface->format->Amask))==NULL) mysdl_error("SDL_CreateRGBSurfaceFrom");
            makeTexture(saveSurface);
            SDL_FreeSurface(saveSurface);
        }
    }
    
    SDL_Texture* operator()()
    { return _texture;}

    SDL_Rect size()
    {
        SDL_Rect rect;
        if (SDL_QueryTexture(_texture,NULL,NULL,&(rect.w), &(rect.h))<0) mysdl_error("SDL_QueryTexture"); 
        return rect;
    }
    
    int width()
    { return size().w; }
    
    int height()
    { return size().h; }

private:

    void makeTexture(SDL_Surface* surface)
    {
        if (sdl())
        {
            if (SDL_LockSurface(surface)<0) mysdl_error("SDL_LockSurface");
            if ((_texture=SDL_CreateTextureFromSurface(sdl()->renderer(),surface))==NULL) mysdl_error("SDL_CreateTextureFromSurface");
            SDL_UnlockSurface(surface);
        }
    }
    
    SDL_Texture* _texture;
};

std::ostream& operator<<(std::ostream& os,const SDL_Rect& r)
{ return os<<r.x<<','<<r.y<<','<<r.w<<','<<r.h; }

#endif
