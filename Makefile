
CC = g++
SRCS = $(wildcard *.cpp)
HEADERS = $(wildcard *.h)

PROGS = $(patsubst %.cpp,%,$(SRCS))

LIBS = -lSDL2 -lSDL2_image -lSDL2_gfx -lSDL2_ttf

all: 	$(PROGS)

%: %.cpp $(HEADERS)
	$(CC) $(CXXFLAGS) -o $@ $< $(LIBS)

clean: 
	rm -f $(PROGS)

PREFIX = /usr/local
.PHONY: install
install: MySDL.h
	cp MySDL.h $(PREFIX)/include
.PHONY: uninstall
uninstall:
	rm -f $(PREFIX)/include/MySDL.h
