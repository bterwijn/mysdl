#include "MySDL.h"

#include <iostream>
using namespace std;

void handleKeys()
{
     const Uint8* currentKeyStates=SDL_GetKeyboardState(NULL); // keyboard state
     if (currentKeyStates[SDL_SCANCODE_UP])
         cout<<"UP"<<endl;
     if (currentKeyStates[SDL_SCANCODE_DOWN])
         cout<<"DOWN"<<endl;
     if (currentKeyStates[SDL_SCANCODE_LEFT])
         cout<<"LEFT"<<endl;
     if (currentKeyStates[SDL_SCANCODE_RIGHT])
         cout<<"RIGHT"<<endl;
     if (currentKeyStates[SDL_SCANCODE_A])
         cout<<"a"<<endl;
     // for keys see: https://wiki.libsdl.org/SDL_Scancode
}

int main (int argc, char** argv)
{
    MySDL::create_window("MySDLTest");
    MyTexture texture=MyTexture("Smiley.png");
    MyTexture screen_grep;    
    SDL_Color color = {255, 255, 255, 255};
    MyTexture text("Hello World!",color);
        
    double x=100,y=200,dx=0.1,dy=0.12,width=40,height=30;
    bool quit=false;
    int step=0;
    while( !quit )
    {
        
        SDL_Event e;
        while( SDL_PollEvent(&e))
        {
            int x, y;
            switch(e.type)
            {
            case SDL_QUIT:
                cout<<"quiting"<<endl;
                quit = true;
                break;
            case  SDL_WINDOWEVENT:
                sdl()->handleEvent(e);
                break;
            case SDL_MOUSEBUTTONDOWN:
                SDL_GetMouseState( &x, &y );
                cout<<"mouse down: "<<x<<","<<y<<endl;
                break;
            case SDL_MOUSEBUTTONUP:
                SDL_GetMouseState( &x, &y );
                cout<<"mouse up: "<<x<<","<<y<<endl;
                break;
            }
        }
        handleKeys();
        
        if (sdl()) SDL_RenderClear(sdl()->renderer()); // clear
        int w,h;
        if (sdl()) sdl()->getSize(w,h);
        
        { // draw texture
            SDL_Rect src={0,0,texture.width()/2,texture.height()};
            SDL_Rect dst={100,20,100,100};
            if (sdl()) SDL_RenderCopy(sdl()->renderer(), texture(), &src, &dst);
        }

        { // draw texture
            SDL_Rect dst={200,20,100,100};
            if (sdl()) SDL_RenderCopy(sdl()->renderer(), texture(), NULL, &dst);
        }

        { // draw texture
            SDL_Rect src={texture.width()/2,0,texture.width(),texture.height()};
            SDL_Rect dst={300,20,100,100};
            if (sdl()) SDL_RenderCopy(sdl()->renderer(), texture(), &src, &dst);
        }

        { // draw texture
            SDL_Rect dst={250,200,text.width(),text.height()};
            if (sdl()) SDL_RenderCopy(sdl()->renderer(), text(), NULL, &dst);
        }

        if (screen_grep()!=nullptr)
        {
            SDL_Rect dst={0,300,200,200};
            if (sdl()) SDL_RenderCopy(sdl()->renderer(), screen_grep(), NULL, &dst);
        }
        
        Col blue=MyColor(0,0,255,255);
        Col white=MyColor(255,255,255,255);
        if (sdl()) boxColor(sdl()->renderer(),x,y,x+width,y+height, blue); // draw filled rectangle
        if (sdl()) rectangleColor(sdl()->renderer(),x,y,x+width,y+height, white); // draw rectangle
        x+=dx;
        y+=dy;
        dy+=0.0001;
        if (x<0) {dx=-dx;x=0;}
        if (x>w-width) {dx=-dx;x=w-width;}
        if (y<0) {dy=-dy;y=0;}
        if (y>h-height) {dy=-dy;y=h-height;}
        
        if (sdl())
        {
            lineColor(sdl()->renderer(),0,0,w,h, white); // draw line
            lineColor(sdl()->renderer(),w,0,0,h, white); // draw line
        
            for (unsigned y=0;y<20;++y)
                for (unsigned x=0;x<30;++x)
                    pixelColor(sdl()->renderer(),x*4,40+y*4, MyColor(255,255,0,255)); // draw point
                           
            filledEllipseColor(sdl()->renderer(), 200,200,50,25, MyColor(255,0,255,255)); // draw ellipse
            ellipseColor(sdl()->renderer(), 200,200,50,25, white);

            if (step==1000)
            {
                screen_grep=MyTexture(sdl()->window());
            }

            SDL_RenderPresent(sdl()->renderer());
            SDL_SetRenderDrawColor(sdl()->renderer(),0,0,0,255); // black background
            SDL_RenderClear(sdl()->renderer());
        }
        step++;
    }
    
    MySDL::destroy_window();
    return EXIT_SUCCESS;
}
